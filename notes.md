
# Homework - 3

## Scenario 1

- create new repository at gitlab
- give to the repository propriate name 
- after repository is created go to the repository
- copy repository path in clone ssh section
- open source control at IDE
- press 'clone repository'
- put there cloned repository path
- press enter

- after repository is cloned to IDE complete your homework  
- open terminal inside current IDE project 
- enter git status
- enter git add . (to add ALL project files)
- enter git commit -m "add project files"
- enter git push
- enter git status
- the end


## Scenario 2

- create new repository at gitlab
- give to the repository propriate name 

- open IDE 
- create new project
- copy completed homework files to current project 
- open terminal inside IDE project 
- enter git init
- enter git add .
- enter git commit -m "initial commit"
- enter git remote add origin git@gitlab.com:user-name/project-name.git (in my case - git remote add origin git@gitlab.com:OleksandrNiefodov/pe26-homework-3.git )
- enter git push -u origin master
- enter git status
- the end 


